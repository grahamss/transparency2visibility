#!/usr/bin/env python

import pandas as pd
from pathlib import Path
import spacy
home = str(Path.home())
nlp = spacy.load('trained')

# The following are for internal use on Windows machines:
# import os
# java_path = "C:/Program Files/Java/jre1.8.0_181/bin/java.exe"
# os.environ['JAVAHOME'] = java_path

#Import text file:

with open('coi.txt', 'r') as f:
    data = f.read().rstrip()

# data = pd.read_csv('coisamp_1000.csv')


def spacy():
    """Return a list of orgs tagged by Spacy NER."""
    author_list = []
    sponsor_list = []
    for ent in nlp(data).ents:
        if ent.label_ == 'ORG':
            sponsor_list.append(ent.text)
        if ent.label_ == 'PERSON':
            author_list.append(ent.text)
    return set(sponsor_list), author_list


def make_coi1df():
    regex_frame = pd.DataFrame([[data,[],[],[]]], columns=list('ABCD'))
    author_list = spacy()[1]
    sponsor_list = spacy()[0]

    try:
        for name in author_list:
            for org in sponsor_list:
                regex_frame = regex_frame.append(regex_frame[
                                                         'A'].str.extractall(
                        '({}|He|She|The authors?)[\s,\-\/\:]+(?:\w+(?!\.\s[A-Z])\W+){{,80}}({})[^\.;]+({})'.format(
                            name, coi_type_1, org)))
        regex_frame.drop(['A', 'B', 'C', 'D'], axis=1, inplace=True)
        regex_frame.drop(0, inplace=True)
        regex_frame.columns = ['Name', 'coi_type', 'coi_source']
        regex_frame['coi_weight'] = 3
        return regex_frame
    except:
        pass

def make_coi2df():
    regex_frame = pd.DataFrame([[data,[],[],[]]], columns=list('ABCD'))
    author_list = spacy()[1]
    sponsor_list = spacy()[0]
    try:
        for name in author_list:
            for org in sponsor_list:
                regex_frame = regex_frame.append(regex_frame[
                    'A'].str.extractall(
                    '({}|He|She|The authors?)[\s,\-\/\:]+(?:\w+(?!\.\s[A-Z])\W+){{,80}}({})[^\.;]+({})'.format(
                        name, coi_type_2, org)))
        regex_frame.drop(['A', 'B', 'C', 'D'], axis=1, inplace=True)
        regex_frame.drop(0, inplace=True)
        regex_frame.columns = ['Name', 'coi_type', 'coi_source']
        regex_frame['coi_weight'] = 2
        return regex_frame
    except:
        pass

def make_coi3df():
    regex_frame = pd.DataFrame([[data,[],[],[]]], columns=list('ABCD'))
    author_list = spacy()[1]
    sponsor_list = spacy()[0]

    try:
        for name in author_list:
            for org in sponsor_list:
                regex_frame = regex_frame.append(regex_frame[
                    'A'].str.extractall(
                    '({}|He|She|The authors?)[\s,\-\/\:]+(?:\w+(?!\.\s[A-Z])\W+){{,15}}({})[^\.;]+({})'.format(
                        name, coi_type_3, org)))
        regex_frame.drop(['A', 'B', 'C', 'D'], axis=1, inplace=True)
        regex_frame.drop(0, inplace=True)
        regex_frame.drop_duplicates(inplace=True)
        regex_frame.columns = ['Name', 'coi_type', 'coi_source']
        regex_frame['coi_weight'] = 1
        return regex_frame
    except:
        pass

#Regexes for the three COI types:
coi_type_1 = r'(?:equity in|(?:owns?|owned|owned by)|patent|financial interest in|employ\w+\W|is (?:CEO|CFO)|is the (?:CEO|CFO)|inventor|found\w+|co-?found\w+)'
coi_type_2 = r'(?:grant|fund\w+\W|support\w+\W|contract\w+\W|collaborat\w+\W|research)'
coi_type_3 = r'(?:consul\w+\W|advi\w+\W\w+\W\w+|honorari\w+\W|fees?|edit\w+\W|travel\w*|member|panel)'

#Write the dataframes to CSV files

final_df = pd.DataFrame()
x=make_coi1df()
if isinstance(x, pd.DataFrame):
    final_df = final_df.append(x)

y=make_coi2df()
if isinstance(y, pd.DataFrame):
    final_df = final_df.append(y)

z=make_coi3df()
if isinstance(z, pd.DataFrame):
    final_df = final_df.append(z)

final_df.to_csv('sample.csv')
for name in spacy()[0]:
    print(name)
