#SPARQL Validator | Validates target hits from coi.pynb against Wiki Data entries

import time
import pandas as pd
from SPARQLWrapper import SPARQLWrapper, JSON
start = time.time()
desired_width = 320
pd.set_option('display.width', desired_width)
pd.set_option("display.max_columns", 10)


#  Builds swap table of common problems; these are swapped out after the initial read from the Excel spreadsheet.
swap_table = {
    "HSC": None,
    "AB": None,
    "Tourette's": None,
    "Systemix": "Novartis",
    "Systemix Novartis": "Novartis",
    "Stem Cells": "StemCells, Inc.",
    "Stem Cells, Inc.": "StemCells, Inc.",
    "Active Biotech": "Active Biotech",
    "Biogen IDEC Inc": "Biogen",
    "Biogen IDEC Inc;": "Biogen",
    "Biogen-Idec": "Biogen",
    "BMS": "BristolMyers Squibb",
    "Discovery Genomics, Inco": "Discovery Genomics",
    "Discovery Genomics, Inc.": "Discovery Genomics",
    "Genentech, Inc.": "Genentech",
    "Celera": "Celera Corporation",
    "NimbleGen Systems": "NimbleGen Systems, Inc.",
    "Roche Pharmaceuticals": "Roche",
    "Roche Biosciences": "Roche",
    "Rinat Labs": "Rinat Neuroscience Corp.",
    "Virco": "Virco Manufacturing Corporation",
    "Zeneca": "AstraZeneca",
    "GSK": "GlaxoSmithKline",
    "Pfizer": "Pfizer Inc."
}
swap_table2 = {
    "Wyeth": 'Q901932'
}


# Pulls ArticleID and company name from the Excel spreadsheet.
file_loc = "inputs/author_table.xlsx"
industry_table = pd.read_excel(file_loc, sheet_name='Sheet2', usecols="A, D", converters={'pmid': str, 'weight': str})  #

# Drops those weird extra lines; we can cut this once we refine the data.
industry_table.dropna(inplace=True)

# Drops duplicate company names from the dataframe.
industry_table = industry_table.drop_duplicates(subset='source', keep='first')


def grab_company_data(table):

    # Creates blank dataframe for storing results
    results = pd.DataFrame()

    # For every index in the initial dataframe...
    for index in table.index:

        # Pull the next row
        pmid = table.loc[index][0]
#        author_name = table.loc[index][1]
#        coi_type = table.loc[index][2]
        i = table.loc[index][1]
#        weight = table.loc[index][4]

        # If there's a better name for the company in the swap table, use that instead.  This is where the machine learning will help us, because these will be better to start with.
        if i in swap_table:
            i = swap_table.get(i)

        if i is None:
            continue

        # Builds Wikidata query using SPARQL.  Takes 'i', the best version of the company name we have so far, and searches for it.  If found, it pulls the better name, the website, and industry info.
        url = 'https://query.wikidata.org/sparql'
        query = """
        PREFIX schema: <http://schema.org/>

        SELECT DISTINCT ?item ?itemLabel ?itemDescription ?official_website ?industry ?industryLabel WHERE {
          ?item ?label \"""" + i + """\"@en.
          ?article schema:about ?item.
          ?article schema:inLanguage "en".
          ?article schema:isPartOf <https://en.wikipedia.org/>.
          OPTIONAL { ?item wdt:P856 ?official_website. }
          OPTIONAL { ?item wdt:P452 ?industry. }
          SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
        } limit 1
        """

        def get_results(endpoint_url, endpoint_query):
            sparql = SPARQLWrapper(endpoint_url)
            sparql.setQuery(endpoint_query)
            sparql.setReturnFormat(JSON)
            try:
                return sparql.query().convert()
            except:
                print("sparql error")
                print(endpoint_query)

        # This is where we actually submit the SPARQL query.
        data = get_results(url, query)

        # The result comes back in JSON.  This chunk parses the JSON to pull the ID, company name, and company URL.
        if not data['results']['bindings']:

            # If there's no result from Wikidata, just fill in the dataframe with what we have.
            results = results.append(
                {'co_name': i,
#                 'weight': weight,
#                 'industry': 'N/A',
                 'url': 'N/A',
                 'pmid': pmid,
#                 'author_name': author_name,
#                 'coi_type': coi_type
                 },
                ignore_index=True)

        else:
            # If there *is* a result from Wikidata, plug in the new information.
            for item in data['results']['bindings']:
                try:
                    company = item['itemLabel']['value']
                except:
                    company = 'N/A'
                try:
                    uid = item['item']['value']
                except:
                    uid = 'N/A'
                try:
                    industry = item['industry']['value']
                except:
                    industry = 'N/A'
                try:
                    description = item['itemDescription']['value']
                except:
                    description = 'N/A'
                try:
                    industrylabel = item['industryLabel']['value']
                except:
                    industrylabel = 'N/A'
                try:
                    url = item['official_website']['value']
                except:
                    url = 'N/A'

                # Append the result to the "results" dataframe.
                results = results.append(
                    {
                        'pmid': pmid,
#                        'author_name': author_name,
#                        'coi_type': coi_type,
#                        'weight': weight,
#                        'original': i,
                        'co_name': company,
#                        'uid': uid,
#                        'description': description,
#                        'industry': industry,
#                        'industry_type': industrylabel,
                        'url': url},
                    ignore_index=True)


#    results.sort_values(by=['co_name'], inplace=True)
    #    results.drop_duplicates(subset=['company'], inplace=True)
#    results = results[['pmid', 'author_name', 'coi_type', 'weight', 'original', 'co_name', 'url', 'industry_type']]
#    results = results[['pmid', 'author_name', 'coi_type', 'weight', 'co_name', 'url', 'industry_type']]
    results = results[['pmid', 'co_name', 'url']]

#    results = results[results['industry_type'] == 'pharmaceutical industry']
    results = results.drop_duplicates(subset='co_name', keep='first')
    results = results.sort_values(by=['co_name'])

    print(len(results))
#    results.to_excel("/outputs/output.xlsx")
    opencorporate_parse(results)

def opencorporate_parse(table):
    print(table)
    import urllib.request, json
    with urllib.request.urlopen("https://api.opencorporates.com/v0.4/companies/search?q=barclays+bank") as url:
        data = json.loads(url.read().decode())
        print(data)


grab_company_data(industry_table)
#opencorporate_parse(results)
end = time.time()
print(end - start)
