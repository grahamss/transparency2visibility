#!/usr/bin/env python

import pandas as pd
import sqlite3
# import nltk
from pathlib import Path
# import itertools
# from nltk import StanfordNERTagger
import numpy as np
import multiprocessing as mp
import spacy
import re
# import csv
home = str(Path.home())
nlp = spacy.load('en_core_web_lg')
nlp.from_disk('trained')

# The following are for internal use on Windows machines:
# import os
# java_path = "C:/Program Files/Java/jre1.8.0_181/bin/java.exe"
# os.environ['JAVAHOME'] = java_path
# samp_conn = sqlite3.connect('coisamp.db')
# cur = samp_conn.cursor()
# samp = pd.read_sql("Select * from article_meta_data", samp_conn, index_col='pmid')

conn = sqlite3.connect('pubmed.db')
cur = conn.cursor()
author_df = pd.read_sql("Select * from authors where pmid in (select pmid from coisamp)", conn, index_col='pmid')
cois = pd.read_sql("Select * from article_meta_data where pmid in (select pmid from coisamp)", conn, index_col='pmid')
# author_df = pd.read_sql("SELECT * FROM authors WHERE pmid in (SELECT pmid FROM article_meta_data WHERE lang = 'eng' AND coi != 'NA')", conn, index_col='pmid')
# cois = pd.read_sql("Select * from article_meta_data WHERE lang = 'eng' AND coi != 'NA'", conn, index_col='pmid')
# cois = cois[:91375]  # This excludes an entry with bad index formatting. Could be fixed eventually.

# cois_short = cois[cois['coi'].str.count('\w+') < 10]
# cois_short.to_csv('cois_short.csv')

cois = cois.replace({'\([A-Z]{3,4}\)': ''}, regex=True) # Removes acronyms like (ABC) and (ABCD)
cois = cois.drop_duplicates('coi') # Drops duplicate statements
cois = cois[cois['coi'].str.count('\w+') > 10] # Drops all statements with fewer than 10 words

pmids = cois.index.tolist()

author_df.index = pd.to_numeric(author_df.index)

author_df['Xavier Aristotle'] = author_df['first'] + ' ' + author_df['last']
author_df['X Aristotle'] = author_df['first'].str[0] + ' ' + author_df['last']
author_df['X. Aristotle'] = author_df['first'].str[0] + '.' + ' ' + author_df['last']
author_df['Xavier Bartholomew Aristotle'] = author_df['first'].str.split(' ').str[0] + ' ' + \
                                            author_df['first'].str.split(' ').str[1] + ' ' + author_df['last']
author_df['Xavier B. Aristotle'] = author_df['first'].str.split(' ').str[0] + ' ' + \
                                   author_df['first'].str.split(' ').str[1].str[0] + '.' + ' ' + author_df[
                                       'last']
author_df['XB Aristotle'] = author_df['first'].str.split(' ').str[0].str[0] + \
                            author_df['first'].str.split(' ').str[1].str[0] + ' ' + author_df['last']
author_df['X.B. Aristotle'] = author_df['first'].str.split(' ').str[0].str[0]+'.'+author_df['first'].str.split(' ').str[1].str[0]+'.'+' '+author_df['last']
author_df['X.B.A.'] = author_df['first'].str.split(' ').str[0].str[0] + '.' + \
                      author_df['first'].str.split(' ').str[1].str[0] + '.' + author_df['last'].str[0] + '.'
author_df['XBA'] = author_df['first'].str.split(' ').str[0].str[0] + \
                   author_df['first'].str.split(' ').str[1].str[0] + author_df['last'].str[0]
author_df['XA'] = author_df['first'].str[0] + author_df['last'].str[0]
author_df['X.A.'] = author_df['first'].str[0] + '.' + author_df['last'].str[0] + '.'
author_df['X. A.'] = author_df['first'].str[0]+'. '+author_df['last'].str[0]+'.'
author_df['Aristotle X.'] = author_df['last']+' '+author_df['first'].str.split(' ').str[0].str[0]

MERGED = cois.merge(author_df, on='pmid', how='left')

MERGED.reset_index(inplace=True)


# Import and set up the Stanford NER tagger:
# jar = 'data_files/StanfordNER/stanford-ner.jar'
# model = 'data_files/StanfordNER/english.all.3class.distsim.crf.ser'
# NER_TAGGER = StanfordNERTagger(model, jar, encoding='utf8')
#
#
# def stanford(z):
#     """Return a list of orgs tagged by Stanford NER."""
#     for row in z.loc[:, 'coi']:
#         x = nltk.word_tokenize(row)
#         y = (NER_TAGGER.tag(x))
#         for tag, chunk in itertools.groupby(y, lambda x: x[1]):
#             if "ORG" in tag:
#                 sponsor_list_stanford.append(" ".join(w for w, t in chunk))
#     return sponsor_list_stanford



def spacy(z):
    """Return a list of orgs tagged by Spacy NER."""
    sponsor_list_spacy = []
    x = nlp(z.loc['coi'])
    for ent in x.ents:
        if ent.label_ == 'ORG':
            sponsor_list_spacy.append(ent.text)
    return sponsor_list_spacy

# def make_authorlist(z):
#     author_list_temp = []
#     for column in z.dropna():
#         author_list_temp.append(column)
#     return author_list_temp[11:]

def clean_sponsorlist(z):
    z = [i.replace('(', '') for i in z]
    z = [i.replace(')', '') for i in z]
    z = [re.sub('the\s', '', i) for i in z]
    z = [re.sub('\sCorporation', '', i) for i in z]
    z = [re.sub('\sCorp\.', '', i) for i in z]
    z = [re.sub('\sCo\.', '', i) for i in z]
    z = [re.sub('\sAG', '', i) for i in z]
    z = [re.sub('\sInst', '', i) for i in z]
    z = [re.sub(',\sInc.*', '', i) for i in z]
    z = [re.sub('\sLLC', '', i) for i in z]
    z = [re.sub('\sllc', '', i) for i in z]
    z = [re.sub('\sInc.*', '', i) for i in z]
    z = [re.sub('\s(P|p)harma.*', '', i) for i in z]
    z = [re.sub('\sCanada.*', '', i) for i in z]
    z = [re.sub('\sU\.?K.*', '', i) for i in z]
    z = [re.sub('AB\s', '', i) for i in z]
    # sponsor_list = [re.sub(r'\b\w{1,2}\b', '', i) for i in sponsor_list]
    manual_exclusions = ['AG', 'LLC', 'llc', 'LLC.', 'Inc', 'Inc.', 'INC', 'Inc', 'Incorporated', 'AB']
    z = [i for i in z if i not in manual_exclusions]
    z = [i for i in z if i not in author_list]
    z = set(z)

    return z

def make_coi1df():
    """Return a dataframe of COI type 1 disclosures.

    Iterate over each row in the main dataframe.

    In each row, iterate over each organization in the list of all
    orgs/sponsors.

    Check if disclosure statement in row contains any org from list.

    If so, run it through the regex (see detail below).

    helper_temp allows us to standardize author name format and add PMID.

    Final coi_df drops non-needed columns.
    """

    regex_temp = pd.DataFrame()
    helper_temp = pd.DataFrame()

    for i in merged.index:
        sponsor_list = spacy(merged.loc[i])
        sponsor_list = clean_sponsorlist(sponsor_list)
        if len(sponsor_list) > 0:
            for sponsor in sponsor_list:
                try:
                    for z, name_permutation in merged.loc[i].dropna().loc['Xavier Aristotle':].iteritems():
                        if merged.loc[i].str.contains(
                                '({})[\s,\-\/\:]+(?:(\w+(?!\.\s[A-Z])\W+)|(\w+\.\s(He|She|The authors?)\W+)){{,80}}({})[^\.;]+({})'.format(
                                        name_permutation, coi_type_1, sponsor)).any() == True:
                            regex_temp = regex_temp.append(merged.loc[i].str.extractall(
                                '({}|He|She|The authors?)[\s,\-\/\:]+(?:\w+(?!\.\s[A-Z])\W+){{,80}}({})[^\.;]+({})'.format(
                                    name_permutation, coi_type_1, sponsor)))
                            helper_temp = helper_temp.append(merged.loc[i].loc['pmid':'Xavier Aristotle'])
                except:
                    pass
    regex_temp.reset_index(inplace=True)
    try:
        regex_temp = regex_temp[regex_temp['match'] == 0]
    except:
        pass
    coi1_df = pd.concat([helper_temp.reset_index(), regex_temp.reset_index()], axis=1).drop(
        ['match', 'level_0', 0, 'i_x', 'lang', 'jid', 'coi', 'title', 'abs', 'iso', 'pdat', 'j', 'i_y', 'last', 'first', 'initial', 'orcid'], axis=1).set_index('index')

    return coi1_df



def make_coi2df():
    """Return a dataframe of COI type 2 disclosures.

    Iterate over each row in the main dataframe.

    In each row, iterate over each organization in the list of all
    orgs/sponsors.

    Check if disclosure statement in row contains any org from list.

    If so, run it through the regex (see detail below).

    helper_temp allows us to standardize author name format and add PMID.

    Final coi_df drops non-needed columns.
    """

    regex_temp = pd.DataFrame()
    helper_temp = pd.DataFrame()

    for i in merged.index:
        sponsor_list = spacy(merged.loc[i])
        sponsor_list = clean_sponsorlist(sponsor_list)
        if len(sponsor_list) > 0:
            for sponsor in sponsor_list:
                try:
                    for z, name_permutation in merged.loc[i].dropna().loc['Xavier Aristotle':].iteritems():
                        if merged.loc[i].str.contains(
                                '({})[\s,\-\/\:]+(?:(\w+(?!\.\s[A-Z])\W+)|(\w+\.\s(He|She|The authors?)\W+)){{,80}}({})[^\.;]+({})'.format(
                                    name_permutation, coi_type_2, sponsor)).any() == True:
                            regex_temp = regex_temp.append(merged.loc[i].str.extractall(
                                '({}|He|She|The authors?)[\s,\-\/\:]+(?:\w+(?!\.\s[A-Z])\W+){{,80}}({})[^\.;]+({})'.format(
                                    name_permutation, coi_type_2, sponsor)))
                            helper_temp = helper_temp.append(merged.loc[i].loc['pmid':'Xavier Aristotle'])
                except:
                    pass
    regex_temp.reset_index(inplace=True)
    try:
        regex_temp = regex_temp[regex_temp['match'] == 0]
    except:
        pass
    coi2_df = pd.concat([helper_temp.reset_index(), regex_temp.reset_index()], axis=1).drop(
        ['match', 'level_0', 0, 'i_x', 'lang', 'jid', 'coi', 'title', 'abs', 'iso', 'pdat', 'j', 'i_y', 'last', 'first', 'initial', 'orcid'], axis=1).set_index('index')

    return coi2_df



def make_coi3df():
    """Return a dataframe of COI type 3 disclosures.

    Iterate over each row in the main dataframe.

    In each row, iterate over each organization in the list of all
    orgs/sponsors.

    Check if disclosure statement in row contains any org from list.

    If so, run it through the regex (see detail below).

    helper_temp allows us to standardize author name format and add PMID.

    Final coi_df drops non-needed columns.
    """

    regex_temp = pd.DataFrame()
    helper_temp = pd.DataFrame()

    for i in merged.index:
        sponsor_list = spacy(merged.loc[i])
        sponsor_list = clean_sponsorlist(sponsor_list)
        if len(sponsor_list) > 0:
            for sponsor in sponsor_list:
                try:
                    for z, name_permutation in merged.loc[i].dropna().loc['Xavier Aristotle':].iteritems():
                        if merged.loc[i].str.contains(
                                '({})[\s,\-\/\:]+(?:(\w+(?!\.\s[A-Z])\W+)|(\w+\.\s(He|She|The authors?)\W+)){{,80}}({})[^\.;]+({})'.format(
                                    name_permutation, coi_type_3, sponsor)).any() == True:
                            regex_temp = regex_temp.append(merged.loc[i].str.extractall(
                                '({}|He|She|The authors?)[\s,\-\/\:]+(?:\w+(?!\.\s[A-Z])\W+){{,80}}({})[^\.;]+({})'.format(
                                    name_permutation, coi_type_3, sponsor)))
                            helper_temp = helper_temp.append(merged.loc[i].loc['pmid':'Xavier Aristotle'])
                except:
                    pass
    regex_temp.reset_index(inplace=True)
    try:
        regex_temp = regex_temp[regex_temp['match'] == 0]
    except:
        pass
    coi3_df = pd.concat([helper_temp.reset_index(), regex_temp.reset_index()], axis=1).drop(
        ['match', 'level_0', 0, 'i_x', 'lang', 'jid', 'coi', 'title', 'abs', 'iso', 'pdat', 'j', 'i_y', 'last', 'first', 'initial', 'orcid'], axis=1).set_index('index')

    return coi3_df


    # Regex explanation:
    #
    # ({})[\s,\-\/\:\.]+ --> checks if name permuation is followed by a space, comma, hyphen, slash, colon, as we'd expect based on standard syntax.
    #
    # (?:\w+(?!\.\s[A-Z])\W+){{,37}}({}) --> checks if disclosure type exists within 0-37 word (w+) and non-word (W+) combinations of name permutation. ...
    # ... The part between w+ and W+ is a negative lookahead that breaks the query if after a word match it encounters a sequence of period-space-capital letter, i.e., a sentence break. ...
    # ... This is because we don't expect any single author's COI disclosure to span multiple sentences; having it break at sentence end allows us to widen the syntactic range between ...
    # ... name permutation and disclosure type significantly, without encountering false positives where author x gets matched to author y's disclosure.
    # Keyword lookup 'Mysore' provides an entry that exemplifies why this matters.
    #
    # [^\.;]+({}) --> matches the name permutation + disclosure type to sponsor as long as there's no period or semi-colon between the two.
    # This could be tweaked with the more complex regex above, but currently, I don't see a reason to control the syntactic range (number of words, characters) between ...
    # ... disclosure type and sponsor more stringently than this.


def chunks(l, n):
    """Create number ranges for even chunks."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

#Divide dataframe into slices for multiprocessing:
#slices = list(chunks(range(0,len(MERGED)), 1000))

#Run each slice, first through NER tagger and then regex:
#for slice in slices:
merged = MERGED#[list(slice)[0]:list(slice)[-1]]
    # speedy_merged = merged.loc[:, :'coi'].drop_duplicates(['pmid'])
    # sponsor_list_stanford = []
    # sponsor_list_spacy = []

    # if __name__ == "__main__":
    #     with mp.Pool(40) as pool:
    #         sponsor_list_stanford.append(pool.map(stanford,
    #             np.array_split(speedy_merged,50)))
    # sponsor_list_stanford = list(itertools.chain.from_iterable(*sponsor_list_stanford))

    # if __name__ == "__main__":
    #     with mp.Pool(10) as pool:
    #         sponsor_list_spacy.append(pool.map(spacy,
    #             np.array_split(speedy_merged,50)))
    # sponsor_list_spacy = list(itertools.chain.from_iterable(*sponsor_list_spacy))
    #
    # #Following lines clean the list of orgs:
author_list = []
for column in merged:
    author_list.append(merged[column].tolist())

author_list = [item for sublist in author_list[11:] for item in sublist]
    # sponsor_list = sponsor_list_spacy# + sponsor_list_stanford
    # sponsor_list = [i.replace('(', '') for i in sponsor_list]
    # sponsor_list = [i.replace(')', '') for i in sponsor_list]
    # sponsor_list = [re.sub('the\s', '', i) for i in sponsor_list]
    # sponsor_list = [re.sub('\sCorporation', '', i) for i in sponsor_list]
    # sponsor_list = [re.sub('\sCorp\.', '', i) for i in sponsor_list]
    # sponsor_list = [re.sub('\sCo\.', '', i) for i in sponsor_list]
    # sponsor_list = [re.sub('\sAG', '', i) for i in sponsor_list]
    # sponsor_list = [re.sub('\sInst', '', i) for i in sponsor_list]
    # sponsor_list = [re.sub(',\sInc.*', '', i) for i in sponsor_list]
    # sponsor_list = [re.sub('\sLLC', '', i) for i in sponsor_list]
    # sponsor_list = [re.sub('\sllc', '', i) for i in sponsor_list]
    # sponsor_list = [re.sub('\sInc.*', '', i) for i in sponsor_list]
    # sponsor_list = [re.sub('\s(P|p)harma.*', '', i) for i in sponsor_list]
    # sponsor_list = [re.sub('\sCanada.*', '', i) for i in sponsor_list]
    # sponsor_list = [re.sub('\sU\.?K.*', '', i) for i in sponsor_list]
    # sponsor_list = [re.sub('AB\s', '', i) for i in sponsor_list]
    # # sponsor_list = [re.sub(r'\b\w{1,2}\b', '', i) for i in sponsor_list]
    # manual_exclusions = ['AG', 'LLC', 'llc', 'LLC.', 'Inc', 'Inc.', 'INC', 'Inc', 'Incorporated', 'AB']
    # sponsor_list = [i for i in sponsor_list if i not in manual_exclusions]
    # sponsor_list = [i for i in sponsor_list if i not in author_list]
    # sponsor_list = set(sponsor_list)

    #Regexes for the three COI types:
coi_type_1 = r'(?:equity in|(?:owns?|owned|owned by)|patent|financial interest in|employ\w+\W|is (?:CEO|CFO)|is the (?:CEO|CFO)|inventor|found\w+|co-?found\w+)'
coi_type_2 = r'(?:grant|fund\w+\W|support\w+\W|contract\w+\W|collaborat\w+\W|research)'
coi_type_3 = r'(?:consul\w+\W|advi\w+\W|honorari\w+\W|fees?|edit\w+\W|travel\w*|member|panel)'

results_1 = make_coi1df()
results_1.drop_duplicates(inplace=True)
results_1.columns = ['Name', 'Article ID', 'COI_type', 'COI_source']
results_1['COI_weight'] = 3
coi1_pmids = results_1['Article ID'].tolist()
coi1_missing = list(set(pmids) - set(coi1_pmids))
with open('coi1_missing.txt', 'w') as f:
   for item in coi1_missing:
       f.write("%s\n" % item)

results_2 = make_coi2df()
results_2.drop_duplicates(inplace=True)
results_2.columns = ['Name', 'Article ID', 'COI_type', 'COI_source']
results_2['COI_weight'] = 2
coi2_pmids = results_2['Article ID'].tolist()
coi2_missing = list(set(pmids) - set(coi2_pmids))
with open('coi2_missing.txt', 'w') as f:
    for item in coi2_missing:
        f.write("%s\n" % item)

results_3 = make_coi3df()
results_3.drop_duplicates(inplace=True)
results_3.columns = ['Name', 'Article ID', 'COI_type', 'COI_source']
results_3['COI_weight'] = 1
coi3_pmids = results_3['Article ID'].tolist()
coi3_missing = list(set(pmids) - set(coi3_pmids))
with open('coi3_missing.txt', 'w') as f:
    for item in coi3_missing:
        f.write("%s\n" % item)

# results_1.to_csv('soutput_coi3_{}.csv'.format(merged.index[0]))
# results_2.to_csv('soutput_coi2_{}.csv'.format(merged.index[0]))
# results_3.to_csv('soutput_coi1_{}.csv'.format(merged.index[0]))
