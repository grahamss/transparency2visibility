# Transparency to Visibility (T2V)
The T2V project aims to develop a toolkit that can aid humanities researchers by providing an easy-to-customize automated framework for converting unstructured text into nodes and edges, capturing the relationship among nodes using a combination of Named Entity Recognition, machine learning, and regular expressions. As a test data set, T2V uses conflict of interest statements in medical publishing; these statements are only minimally structured, but contain relationships among writers and agencies that, while obvious to human readers, can be a challenge to capture in a database.

## Conflict Metrics Project Homepage
All of these tools can be accessed and utilized to visualize networks of conflict of interest in biomedical publishing at the public-facing portal, [Conflict Metrics](http://conflictmetrics.com).

## Dataset
Our data comes from the MEDLINE database, an online biomedical and life sciences bibliographic database indexing more than 30 million journal articles, books, and scholarly reports. We downloaded all MEDLINE XML files via the FTP locker. We then used a customized XML parser to load selected data on each of the indexed publications into a local database that would support our project. In our custom database, each article is represented across four tables linked by a common PMID (or PubMed ID), which is also the index used by PubMed, a search engine for accessing MEDLINE database provided by the US National Institutes of Health. Articles are available at https://www.ncbi.nlm.nih.gov/pubmed/[insertPMID].

## COI Parsers
Using a subset of MEDLINE data with attached COI statements, we developed two variants of the T2V parser: the first (PML COI Parser) uses a combination of machine-learning enhanced named-entity recognition (NER) tagging and a conflict type dictionary to identify nodes (sponsors and authors) and edges (reported relationships). The second version (HMA Parser) uses PubMed/MEDLINE author metadata to improve overall parser performance. The COI classification dictionary is based on the International Committee of Medical Journal Editors (ICMJE) standardized conflicts of interest disclosure form. Our dictionary schema organizes these categories into three levels: 

- Type 1 COI: personal fees and non-financial support 
- Type 2 COI: grant and research support
- Type 3 COI: stock ownership and employment 

More complete details are available in the T2V Whitepaper. 

## Visualization App
The [T2V Visualization App](t2viz.R) is an R Shiny app that creates dynamic, manipulable COI network visualizations. This app runs primarily on the VisNework package wich uses the vis.js javascript library and allows for user interactivity controls for display. Users can zoom in and out on the network, drag-and-drop nodes, and highlight network neighborhoods of interest. Additionally, we leveraged reactive functionality in the R Shiny framework to allow for increased user control of network visualizations. Specifically, we added an out-degree filter and the ability to change the network visualization algorithm.

![T2V App Screen Shot](images/shiny-app.png)

## Credits
- S. Scott Graham, University of Texas at Austin
- Zoltan P. Majdik, North Dakota State University
- Dave Clark, University of Wisconsin-Milwaukee

## Funding 
![National Endowment for the Humanities](images/neh2.png)
![XSEDE](images/xsede.png)
